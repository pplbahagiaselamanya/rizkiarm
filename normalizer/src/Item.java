import java.text.*;

/**
 * class Item
 * stores an item information (still from PDB)
 * can normalize the item
 *
 */
class Item
{
	String sku;
	String brandName;
	String productName;
	int packagingQty;
	double sizeNet;
	String unit;
	double height;
	double width;
	double depth;
	String naturalUnit;
	double weight; // diff with size_net?
	boolean sellNatural;
	String country;
	String storeID;
	double price;

	/**
	 * constructor Item
	 * @param String[] strs: array of String from PDB
	 *
	 */

	Item(String s)
	{
		String strs[] = s.split(";");
		try
		{
			if (strs.length < 14 || check(strs[0]) || check(strs[1] + strs[2]) || check(strs[3]) || check(strs[4]) ||
				check(strs[5]) || check(strs[6]) || check(strs[7]) || check(strs[8]) ||
				check(strs[12]) || check(strs[13]))
			{
				sku = null;
			}
			else
			{
				sku = strs[0].trim();
				brandName = strs[1].trim();
				productName = strs[2].trim();
				packagingQty = Integer.parseInt(strs[3]);
				sizeNet = Parser.parseDouble(strs[4]);
				unit = strs[5].trim();
				height = Parser.parseDouble(strs[6]);
				width = Parser.parseDouble(strs[7]);
				depth = Parser.parseDouble(strs[8]);
				naturalUnit = strs[9].trim();
				weight = (strs[10].isEmpty()) ? 0 : Parser.parseDouble(strs[10]);
				sellNatural = strs[11].trim().equalsIgnoreCase("yes");
				country = strs[12].trim();
				storeID = strs[13].trim();
			}
		}
		catch (Exception e)
		{
			// System.out.println(">>>>>> " + strs.length + " hahaha " + s);
			// e.printStackTrace();
		}
	}

	boolean check(String s)
	{
		if (s == null) return false;
		return s.isEmpty();
	}

	/**
	 * void normalize
	 * normalize PDB item to NormItem
	 */
	public NormItem normalize()
	{
		return new NormItem(this);
	}
	
}
