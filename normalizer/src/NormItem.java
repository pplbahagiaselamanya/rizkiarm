/**
 * class NormItem
 * stores normalized items converted from Item
 */

class NormItem
{
	String sku;
	String name;
	double sizeNet;
	String unit;
	double height;
	double width;
	double depth;
	String country;
	String storeID;
	double price;

	final double EPS = 1e-9;

	NormItem(Item item)
	{
		this.sku = item.sku;

		//TODO: normalize name using some string algorithms
		// - Knuth Morris Pratt algorithm
		// - Rabin Karp
		// - Suffix Data Structures (?)
		// - Simpler: String Alignment / Edit Distance 
		this.name = item.brandName + "$" + item.productName;

		//TODO: determine unit
		if (item.sellNatural)
		{
			this.sizeNet = item.weight;
			this.unit = item.unit;
			if (unit.equalsIgnoreCase("gr") || unit.equalsIgnoreCase("gram") || unit.equalsIgnoreCase("g"))
			{
				this.sizeNet /= 1000.0;
				this.unit = "kg";
			}
		}
		else
		{
			this.sizeNet = item.sizeNet;
			this.unit = item.unit;
			if (unit.equalsIgnoreCase("ml"))
			{
				this.sizeNet /= 1000.0;
				this.unit = "l";
			}
			else
			if (unit.equalsIgnoreCase("gr") || unit.equalsIgnoreCase("gram") || unit.equalsIgnoreCase("g"))
			{
				this.sizeNet /= 1000.0;
				this.unit = "kg";
			}
		}

		this.sizeNet *= item.packagingQty;

		this.height = item.height;
		this.width = item.width;
		this.depth = item.depth;
		this.country = item.country;
		this.storeID = item.storeID;
		this.price = item.price;
	}


	/**
	 * bool isSame
	 * @param NormItem other: other item to be compared with
	 * checks whether two items are "same"
	 *
	 */
	boolean isSame(NormItem other)
	{
		if (!sku.equalsIgnoreCase(other.sku))
			return false;

		if (!country.equalsIgnoreCase(other.country))
			return false;

		// no need to compare store

		// assumes that the productNames have been processed by normalizeName()
		if (!name.equalsIgnoreCase(other.name))
			return false; // actually still need to check if there are some typos

		// checks if all dimensions' ratios are the same
		double ratio[] = {height / other.height, width / other.width, depth / other.depth};
		if (Math.abs(ratio[0] - ratio[1]) < EPS && Math.abs(ratio[1] - ratio[2]) < EPS)
		{
			return true;
		}
		else return false;

	}
}

