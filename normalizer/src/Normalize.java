import java.util.*;
import java.io.*;
import java.text.*;

/**
 * class Normalize
 *
 */

public class Normalize
{
	public static void main(String[] args) throws IOException
	{
		Reader.dummy();
		Item item;
		// ArrayList<NormItem> items = new ArrayList<NormItem>();
		try
		{
			while ((item = Reader.getItem()) != null)
			{
				if (item.sku == null)
				{
					// System.out.println("-----");
					continue;
				}
				NormItem normItem = item.normalize();
				// items.add(normItem);
				System.out.println(normItem.sku + ";" + normItem.name + ";" + new DecimalFormat("#0.0000").format(normItem.sizeNet) + ";" + normItem.unit + ";" + normItem.height + ";" + normItem.width + ";" + normItem.depth + ";" + normItem.country + ";" + normItem.storeID + ";" + normItem.price);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		// now items contains all the normalized items

	}
}