/**
 * class Parser
 * to parse input to specified formats
 */

class Parser
{

	private static String formatStringDouble(String s)
	{
		String ret = "";
		for (char c : s.toCharArray())
		{
			if (c != ',')
				ret += c;
		}
		return ret;
	}

	public static double parseDouble(String s)
	{
		return Double.parseDouble(formatStringDouble(s));
	}
}