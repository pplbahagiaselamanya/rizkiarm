import java.io.*;

/**
 * class Reader
 * get PDB items from file and returns as an Item (without normalization)
 *
 */

class Reader
{
	private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	public static void dummy() throws IOException
	{
		br.readLine();
	}

	public static Item getItem() throws IOException
	{
		String s = br.readLine();
		if (s != null)
			return new Item(s);
		return null;
	}
}